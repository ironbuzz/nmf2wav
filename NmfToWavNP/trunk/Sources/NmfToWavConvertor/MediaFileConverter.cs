using System;
using System.Collections;
using System.IO;
using NiceApplications.Playback.FileTypes.LocalFileInfo;
using NiceApplications.Playback.FileTypes.Nmf.Common;
using NiceApplications.Playback.FileTypes.Nmf.Data;
using NiceApplications.Playback.FileTypes.Nmf.Logic;
using NiceApplications.Playback.FileTypes.WAV;
using NiceApplications.Playback.InternalCommon;
using NiceApplications.Playback.MediaServices.Logic;
using NiceApplications.Playback.PlayList.Logic;
using NiceApplications.Playback.Streaming.Common;
using NiceApplications.Playback.Utils;

namespace Nmf2WavConvertor
{
    /// <summary>
    /// Summary description for MediaConverter.
    /// </summary>
    public class MediaFileConverterMy
    {
        #region Nmf To Vox
        /// <summary>
        /// Convert an NMF file in to a VOX file in the given format.
        /// Only the firts voice stream found in the NMF is saved in the VOX file.
        /// </summary>
        static public void NmfToVox(string nmfFileName, string voxFileName, CompressionType voxCompression)
        {
            // Get first voice media from NMF file
            Media voiceMedia = GetNmfVoiceMedia(nmfFileName);
            if (voiceMedia == null)
                throw new Exception("No voice media were found in NMF file");

            // Create the vox writer
            var voxWriter = new BinaryWriter(
                new FileStream(voxFileName, FileMode.Create, FileAccess.Write));

            // Read from NMF & write to the VOX file
            var nmfFetcher = new NmfFileFetcher();
            try
            {
                nmfFetcher.Init(voiceMedia);
                nmfFetcher.StartFetch(0);

                // Get first packet from NMF
                bool isFetchDone;
                var audioPacket = (IAudioPacket)nmfFetcher.GetPacket(out isFetchDone);

                VoiceConverter voiceConverter = null;
                if (!isFetchDone)
                {
                    voiceConverter = new VoiceConverter(
                        false, (CompressionType)audioPacket.Compression, false, voxCompression);
                }

                while (!isFetchDone)
                {
                    // Convert packet and write to VOX file
                    voxWriter.Write(voiceConverter.Convert(audioPacket, voiceMedia));
                    // Get next NMF packet
                    audioPacket = (IAudioPacket)nmfFetcher.GetPacket(out isFetchDone);
                }
            }
            finally
            {
                voxWriter.Close();
                nmfFetcher.Clear();
            }
        }
        #endregion

        #region Nmf To Wav
        /// <summary>
        /// Convert an NMF file in to a WAV file with the specified format.
        /// Only the firts voice stream found in the NMF is saved in the WAV.
        /// </summary>
        static public void NmfToWav(string nmfFileName, string wavFileName,
            bool isWavUncompressed, CompressionType wavCompression)
        {
            // Get first voice media from NMF file
            Media voiceMedia = GetNmfVoiceMedia(nmfFileName);
            if (voiceMedia == null)
                throw new Exception("No voice media were found in NMF file");

            // Create wav writer
            var wavWriter = CreateWavWriter(wavFileName, isWavUncompressed, wavCompression);

            // Read from NMF & write to WAV
            var nmfFetcher = new NmfFileFetcher();
            try
            {
                nmfFetcher.Init(voiceMedia);
                nmfFetcher.StartFetch(0);

                // Get first packet from NMF
                bool isFetchDone;
                var audioPacket = (IAudioPacket)nmfFetcher.GetPacket(out isFetchDone);

                VoiceConverter voiceConverter = null;
                if (!isFetchDone)
                {
                    voiceConverter = new VoiceConverter(
                        false, (CompressionType)audioPacket.Compression,
                        isWavUncompressed, wavCompression);
                }

                while (!isFetchDone)
                {
                    // Convert packet and write to WAV
                    wavWriter.WriteData(voiceConverter.Convert(audioPacket, voiceMedia));
                    // Get next NMF packet
                    audioPacket = (IAudioPacket)nmfFetcher.GetPacket(out isFetchDone);
                }
            }
            finally
            {
                wavWriter.Close();
                nmfFetcher.Clear();
            }
        }
        #endregion

        #region Wav To Nmf
        /// <summary>
        /// Convert a WAV file in mu-law or a-law format to an NMF file.
        /// </summary>
        static public void WavToNmf(string wavFileName, string nmfFileName, CompressionType nmfCompression)
        {
            const int bufferSize = 8000;
            const int millisecSilenceToAdd = 1;
            BinaryWriter nmfWriter = null;
            CompressionType wavCompression;
            bool isWavUncompressed;
            var listAudioOffsets = new ArrayList();
            var listSilenceOffsets = new ArrayList();
            var listStreamIndexes = new ArrayList();

            // Open WAV file for reading
            var wavReader = new WavFileReader(wavFileName);
            // Get compression 
            if (!CovertWavCompression(wavReader.Compression, out isWavUncompressed, out wavCompression))
                throw new Exception("WAV format cannot be converted to NMF.");
            // Init voice converter
            var voiceConverter = new VoiceConverter(isWavUncompressed, wavCompression, false, nmfCompression);
            // Calculate wav start & end times
            var duration = new TimeSpan(0, 0, 0, 0,
                GetBufferDurationInMilli(wavReader.DataChunkSize, wavReader.AvarageBytesPerSecond));
            duration += TimeSpan.FromMilliseconds(millisecSilenceToAdd);
            DateTime streamEndTime = DateTime.Now;
            DateTime streamStartTime = streamEndTime - duration;

            try
            {
                // Open NMF file for writing
                nmfWriter = new BinaryWriter(new FileStream(
                    nmfFileName, FileMode.Create, FileAccess.ReadWrite, FileShare.None));

                // Write headers
                WriteFileHeaderPacket(nmfWriter, streamStartTime, streamEndTime);
                long streamDescPos = WriteVoiceStreamDescriptorPacket(nmfWriter, streamStartTime, streamEndTime);

                // Write stream audio data packets
                DateTime dataEndTime;
                DateTime dataStartTime = streamStartTime;
                byte[] data = wavReader.ReadBytes(bufferSize);
                long streamPos;
                do
                {
                    if (data.Length == 0)
                        break;
                    // Calculate end time
                    var dataDuration = new TimeSpan(0, 0, 0, 0,
                                                         GetBufferDurationInMilli(data.Length, wavReader.AvarageBytesPerSecond));
                    dataEndTime = dataStartTime + dataDuration;

                    // Convert data compression
                    data = voiceConverter.Convert(data);

                    // Write stream data packet
                    streamPos = WriteVoiceStreamDataPacket(
                        nmfWriter, data, dataStartTime, dataEndTime, nmfCompression,
                        SubType_StreamData_DataType_Voice.AUDIO_DATATYPE);
                    // Add item to audio indexes table
                    AddDataToStreamIndex((int)streamPos, dataStartTime, dataEndTime, listAudioOffsets);

                    // Read next wav buffer
                    data = wavReader.ReadBytes(bufferSize);
                    // Set next buffer start time
                    dataStartTime = dataEndTime;
                }
                while (data.Length > 0);

                // Write stream silence data packet
                dataEndTime = dataStartTime + TimeSpan.FromMilliseconds(millisecSilenceToAdd);
                streamPos = WriteVoiceStreamDataPacket(
                    nmfWriter, null, dataStartTime, dataEndTime, nmfCompression,
                    SubType_StreamData_DataType_Voice.SILENCE_DATATYPE);
                // Add item to silence indexes table
                AddDataToStreamIndex((int)streamPos, dataStartTime, dataEndTime, listSilenceOffsets);

                // Write audio stream index
                streamPos = WriteStreamIndexPacket(
                    nmfWriter, SubType_StreamIndex.AUDIO_INDEXTYPE, listAudioOffsets);
                // Add audio stream index to list
                AddStreamIndexToSubTypeList(SubType_StreamIndex.AUDIO_INDEXTYPE, (int)streamPos, listStreamIndexes);

                // write silence stream index
                streamPos = WriteStreamIndexPacket(
                    nmfWriter, SubType_StreamIndex.SILENCE_INDEXTYPE, listSilenceOffsets);
                // Add silence stream index to list
                AddStreamIndexToSubTypeList(SubType_StreamIndex.SILENCE_INDEXTYPE, (int)streamPos, listStreamIndexes);

                // Write footers
                long streamFooterPos = WriteStreamFooterPacket(
                    nmfWriter, streamStartTime, streamEndTime, listStreamIndexes);
                WriteFileFooterPacket(nmfWriter, streamStartTime, streamEndTime, streamDescPos, streamFooterPos);
            }
            finally
            {
                wavReader.Close();

                if (nmfWriter != null)
                    nmfWriter.Close();
            }
        }
        #endregion

        #region Write Nmf Packet Functions
        static private void WriteFileHeaderPacket(BinaryWriter nmfWriter, DateTime streamStartTime, DateTime streamEndTime)
        {
            var packet = new FileHeaderPacket
                {
                    UniqueID = BasePacket.NMF_UNIQUE_ID,
                    FileVersion = BasePacket.NMF_VERSION,
                    Header =
                        {
                            StreamID = 0,
                            StartTime = streamStartTime,
                            EndTime = streamEndTime,
                            PacketSize = 0,
                            ParametersSize = 0
                        }
                };

            GenericPacketWriter.WritePacket(packet, nmfWriter);            
        }

        static private long WriteVoiceStreamDescriptorPacket(
            BinaryWriter nmfWriter, DateTime streamStartTime, DateTime streamEndTime)
        {
            var packet = new VoiceStreamDescriptorPacket
                {
                    Header =
                        {
                            PacketType = (byte) PacketType.STREAM_DESCRIPTOR,
                            PacketSubType = (short) SubType_StreamDescriptorOrStreamFotter.VOICE,
                            StreamID = 0,
                            StartTime = streamStartTime,
                            EndTime = streamEndTime,
                            PacketSize = 0,
                            ParametersSize = 0
                        }
                };

            return GenericPacketWriter.WritePacket(packet, nmfWriter);
        }

        static private long WriteVoiceStreamDataPacket(
            BinaryWriter nmfWriter, byte[] data, DateTime streamStartTime, DateTime streamEndTime,
            CompressionType compression, SubType_StreamData_DataType_Voice dataType)
        {
            var packet = new VoicStreamDataPacket
                {
                    Header =
                        {
                            PacketType = (byte) PacketType.STREAM_DATA,
                            PacketSubType = (short) dataType,
                            StreamID = 0,
                            StartTime = streamStartTime,
                            EndTime = streamEndTime,
                            PacketSize = 0,
                            ParametersSize = 0
                        }
                };

            // Header

            // Parameters
            packet.AddParameter(GetByteParameter(ParametersType.COMPRESSION, (byte)compression));
            packet.AddParameter(dataType == SubType_StreamData_DataType_Voice.SILENCE_DATATYPE
                                    ? GetByteParameter(ParametersType.FIRST_ACTIVE_BUFFER_FLAG, 1)
                                    : GetByteParameter(ParametersType.FIRST_ACTIVE_BUFFER_FLAG, 0));

            // Data
            packet.Data = data;
            packet.Header.PacketSize = packet.Header.ParametersSize;
            if (data != null)
                packet.Header.PacketSize += (uint)data.Length;

            return GenericPacketWriter.WritePacket(packet, nmfWriter);
        }

        static private long WriteStreamIndexPacket(
            BinaryWriter nmfWriter, SubType_StreamIndex indexType, ArrayList listIndexOffsets)
        {
            bool isTimesUpdated = false;
            DateTime indexStartTime = DateTime.MinValue;
            DateTime indexEndTime = DateTime.MinValue;
            var packet = new StreamIndexPacket
                {
                    Header =
                        {
                            PacketType = (byte) PacketType.STREAM_INDEX,
                            PacketSubType = (short) indexType,
                            StreamID = 0,
                            PacketSize = 0,
                            ParametersSize = 0
                        }
                };

            // Header

            // Data
            foreach (IndexOffset indexOffset in listIndexOffsets)
            {
                // Set stream index times
                if (!isTimesUpdated)
                {
                    indexStartTime = indexOffset.StartTime;
                    indexEndTime = indexOffset.EndTime;
                    isTimesUpdated = true;
                }
                else
                {
                    if (indexOffset.StartTime < indexStartTime)
                        indexStartTime = indexOffset.StartTime;
                    if (indexOffset.EndTime > indexEndTime)
                        indexEndTime = indexOffset.EndTime;
                }

                // Add offset
                packet.AddIndex(indexOffset);
            }
            packet.Header.StartTime = indexStartTime;
            packet.Header.EndTime = indexEndTime;

            return GenericPacketWriter.WritePacket(packet, nmfWriter);
        }

        static private long WriteStreamFooterPacket(
            BinaryWriter nmfWriter, DateTime streamStartTime, DateTime streamEndTime, ArrayList listSubTypeOffsets)
        {
            StreamFooterPacket packet = new StreamFooterPacket();

            // Header
            packet.Header.PacketType = (byte)PacketType.STREAM_FOOTER;
            packet.Header.PacketSubType = 0;
            packet.Header.StreamID = 0;
            packet.Header.StartTime = streamStartTime;
            packet.Header.EndTime = streamEndTime;
            packet.Header.PacketSize = 0;
            packet.Header.ParametersSize = 0;

            // Data
            foreach (IndexSubTypeOffset subTypeOffset in listSubTypeOffsets)
                packet.AddIndex(subTypeOffset);

            return GenericPacketWriter.WritePacket(packet, nmfWriter);
        }

        static private long WriteFileFooterPacket(
            BinaryWriter nmfWriter, DateTime streamStartTime, DateTime streamEndTime,
            long streamDescPos, long streamFooterPos)
        {
            var packet = new FileFooterPacket
                {
                    Header =
                        {
                            PacketType = (byte) PacketType.FILE_FOOTER,
                            PacketSubType = 0,
                            StreamID = 0,
                            StartTime = streamStartTime,
                            EndTime = streamEndTime,
                            PacketSize = 0,
                            ParametersSize = 0
                        }
                };

            // Header

            // Data
            var stream = new IndexStreamID
                {
                    StreamID = 0,
                    LastStreamDescriptorOffset = (int) streamDescPos,
                    StreamFooterOffset = (int) streamFooterPos
                };
            packet.AddIndex(stream);
            packet.Header.PacketSize = (uint)(packet.Header.ParametersSize + packet.Data.Length);

            return GenericPacketWriter.WritePacket(packet, nmfWriter);
        }
        #endregion

        #region Add To Nmf Index Functions
        static void AddDataToStreamIndex(int dataOffset, DateTime dataStartTime,
            DateTime dataEndTime, ArrayList streamIndex)
        {
            var indexOffset = new IndexOffset {Offset = dataOffset, StartTime = dataStartTime, EndTime = dataEndTime};
            streamIndex.Add(indexOffset);
        }

        static void AddStreamIndexToSubTypeList(SubType_StreamIndex subType, int indexOffset, ArrayList subTypeList)
        {
            var subTypeOffset = new IndexSubTypeOffset {IndexSubType = subType, Offset = indexOffset};
            subTypeList.Add(subTypeOffset);
        }
        #endregion

        #region Helper Functions - Wav To Nmf
        static private bool CovertWavCompression(int wavCompression, out bool isUncompressed,
            out CompressionType compression)
        {
            bool bSuccess = true;
            isUncompressed = false;
            compression = CompressionType.PCM_MU_LAW;

            switch (wavCompression)
            {
                case 1: isUncompressed = true; break;
                case 6: compression = CompressionType.PCM_A_LAW; break;
                case 7: compression = CompressionType.PCM_MU_LAW; break;
                default: bSuccess = false; break;
            }

            return bSuccess;
        }
        static private void GetIsEncodeDecodeNeeded(bool isWavUncompressed,
            CompressionType wavCompression, CompressionType nmfCompression,
            out bool isNeedDecoding, out bool isNeedEncoding)
        {
            if (isWavUncompressed)
            {
                // Wav is not compressed - need only encoding
                isNeedDecoding = false;
                isNeedEncoding = true;
            }
            else
            {
                if (wavCompression == nmfCompression)
                {
                    // Wav compression is identical to nmf compression - no need to encode or decode
                    isNeedDecoding = false;
                    isNeedEncoding = false;
                }
                else
                {
                    // Wav and nmf have different compressions - need to encode and decode
                    isNeedDecoding = true;
                    isNeedEncoding = true;
                }
            }
        }
        static private int GetBufferDurationInMilli(int bufferSize, int avarageBytesPerSec)
        {
            double durationInSec = (double)bufferSize / (double)avarageBytesPerSec;
            return System.Convert.ToInt32(durationInSec * 1000);
        }
        static private Parameter GetByteParameter(ParametersType paramType, byte paramValue)
        {
            var byData = new byte[PacketParameters.MAX_DATA_SIZE_TO_READ];
            var dataWriter = new BinaryWriter(new MemoryStream(byData));
            dataWriter.BaseStream.Position = PacketParameters.DataSizeToSkip;
            dataWriter.Write(paramValue);

            var param = new Parameter {ParameterType = paramType, ParameterSize = 1, ParameterData = byData};

            return param;
        }
        #endregion

        #region Helper Functions - Nmf To Wav
        static private Media GetNmfVoiceMedia(string nmfFileName)
        {
            Media voiceMedia = null;

            var nmfFileInfo = new NmfFileInfo();
            nmfFileInfo.Init(nmfFileName);

            for (int i = 0; i < nmfFileInfo.GetStreamCount(); i++)
            {
                StreamInfo streamInfo;
                nmfFileInfo.GetStreamInfo(i, out streamInfo);

                if (streamInfo.Type == MEDIA_TYPE.VOICE)
                {
                    voiceMedia = new Media(null)
                        {
                            Handle = new MMLIdentifier(-1, nmfFileInfo.FilePath),
                            MediaId = i,
                            UseForPlayback = true,
                            MediaType = streamInfo.Type,
                            DisplayStartTime = streamInfo.StreamStartTime,
                            DisplayStopTime = streamInfo.StreamEndTime,
                            ServerStartTime = streamInfo.StreamStartTime,
                            ServerStopTime = streamInfo.StreamEndTime,
                            StartPosition = 0,
                            ServerInfo = null,
                            SourceType = MEDIA_SOURCE_TYPE.SOURCE_FROM_NMF_FILE
                        };

                    break;
                }
            }

            nmfFileInfo.Disconnect();
            return voiceMedia;
        }

        static private WavFileWriter CreateWavWriter(string wavFileName,
            bool isWavUncompressed, CompressionType wavCompression)
        {
            if (isWavUncompressed)
                return new WavFileWriter(wavFileName, 1, 1, 8000, 16);

            switch (wavCompression)
            {
                case CompressionType.PCM_A_LAW: return new WavFileWriter(wavFileName, 6, 1, 8000, 8);
                case CompressionType.PCM_MU_LAW: return new WavFileWriter(wavFileName, 7, 1, 8000, 8);
                default: return null;
            }
        }
        #endregion
    }
}
