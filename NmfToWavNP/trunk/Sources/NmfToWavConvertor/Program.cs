using System;
using System.Diagnostics;
using System.Threading;
using System.Configuration;
using ProfessionalServices.Log;
using System.Threading.Tasks;
using System.Reflection;
using System.ServiceProcess;

namespace Nmf2WavConvertor
{
    class Program
    {
        public const string ServiceName = "Nmf2WavConverterService";
        private static Mutex s_Mutex;
        private static readonly ManualResetEvent m_stop = new ManualResetEvent(false);

        static void Main(string[] args)
        {
            if (Environment.UserInteractive)
            {
                    Start(args);
           
            }
            else
            {
                // running as service
                using (var service = new Service())
                {
                    ServiceBase.Run(service);
                }
            }

        }

        public static void RunService(string[] args)
        {
            Task.Factory.StartNew(() =>
            {
                Start(args);
            });
        }

        public static int Start(string[] args)
        {
            PSLog.ShowConsole = true;
#if _DEBUG
            PSLog.ShowConsole = true;
#endif
            var assemblyPath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var mngr = LicenseManager.GetInstance(System.IO.Path.Combine(assemblyPath, "license.irb"));
            if (!mngr.IsValid)
            {
                PSLog.Current.Error(string.Format("{0}: NMF converter failed to start (license is not valid): {1}", "Nmf2WavConvertor.Main()", mngr.Why));
                return 0;
            }
            Task.Factory.StartNew(() =>
            {
                try
                {
                    if (!mngr.IsPermanent)
                    {
                        mngr.Stop = m_stop;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(string.Format("{0}:error occurred:{1}", "Nmf2WavConvertor.Main()", ex.Message));
                }
            });

            if (mngr.IsValid)
            {
                PSLog.Current.Info(string.Format("License information:"));
                PSLog.Current.Info(string.Format("Licensed for: {0}", mngr.Customer));
                PSLog.Current.Info(string.Format("Number of items licensed for processing: {0}", mngr.NumberOfItems));
                LicenseManager.totalLicensed = mngr.NumberOfItems;
                //RegistryHelper.SetItemsNumValue(0);
                if (RegistryHelper.RegistryKeyExists())
                {
                    int currentNumItems = RegistryHelper.GetItemsNumValue();
                    if (currentNumItems >= mngr.NumberOfItems)
                    {
                        PSLog.Current.Error(string.Format("{0}: NMF converter failed to start (licensed number of items to process is expired): {1}", "Nmf2WavConvertor.Main()", mngr.Why));
                        return 0;
                    }

                    LicenseManager.itemsLeft = mngr.NumberOfItems - currentNumItems;
                    PSLog.Current.Info(string.Format("Number of items left for processing: {0}", LicenseManager.itemsLeft));

                }

                if (!RegistryHelper.RegistryKeyExists())
                {
                    RegistryHelper.CreateRegistryKey();
                    LicenseManager.itemsLeft = mngr.NumberOfItems;
                    PSLog.Current.Info(string.Format("Number of items left for processing: {0}", mngr.NumberOfItems));
                }
            }


            // Used to check if we can create a new mutex

            // The name of the mutex is to be prefixed with Local\ to make
            // sure that its is created in the per-session namespace,
            // not in the global namespace.
            const string mutexName = "{27346FE2-C59B-40c3-8D11-63D00955D93E}";

            try
            {
                // Create a new mutex object with a unique name
                bool newMutexCreated;
                s_Mutex = new Mutex(false, mutexName, out newMutexCreated);

                int iTimeoutProcess;
                int.TryParse(ConfigurationManager.AppSettings["TimeoutProcess"], out iTimeoutProcess);
                if (iTimeoutProcess == 0)
                    iTimeoutProcess = 5;

                bool bRetValue = s_Mutex.WaitOne(TimeSpan.FromMinutes(iTimeoutProcess), true);

                if (bRetValue == false)
                {
                    Reporter.Dispose();
                    return 2;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                //Environment.Exit(1);
                Reporter.Dispose();
                return 1;
            }



            if (args.Length != 3)
            {
                // try to read params from params.txt
                string argString = System.IO.File.ReadAllText(System.IO.Path.Combine(assemblyPath, "params.txt"));
                args = argString.Split(new char[] { ' ' });

                if (args.Length != 3)
                {
                    PSLog.Current.Error("Wrong Parameters list.\nUsage example: Nmf2WavConvertor.exe [source] [destination folder] [number of threads]");
                    Reporter.Dispose();
                    return 1;
                }
            }
            var message = string.Format("Source: {0}, destination: {1}, number of threads: {2}", args[0], args[1], args[2]);
            Reporter.WriteLine(message);
            Reporter.WriteLine("");
            PSLog.Current.Info(message);
            var start = Stopwatch.StartNew();
            try
            {

                var nmf2Wav = new Nmf2Wav();
                nmf2Wav.Nmf2WavMain(args[0], args[1], int.Parse(args[2]));


                s_Mutex.ReleaseMutex();
                var messageFinish = string.Format("Process took :{0} (ms)", start.ElapsedMilliseconds);
                if (nmf2Wav.BResult)
                {
                    start.Stop();
                    PSLog.Current.Info(messageFinish);
                    Reporter.WriteLine(messageFinish);
                    Reporter.Dispose();
                    return 0;
                }
                start.Stop();
                PSLog.Current.Info(messageFinish);
                Reporter.WriteLine(messageFinish);
                Reporter.Dispose();
                return 1;
            }
            catch (Exception ex)
            {
                PSLog.Current.Error("Error occurred: {0}", ex);
                Reporter.Dispose();
                return 1;
            }

        }

        public static void Stop()
        {
            Reporter.Dispose();
            Environment.Exit(0);
        }
    }
}
