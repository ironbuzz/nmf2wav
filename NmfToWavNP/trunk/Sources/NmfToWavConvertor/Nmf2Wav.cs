﻿using System;
using System.IO;
using NiceApplications.Playback.Utils;
using ProfessionalServices.Log;
using System.Threading.Tasks;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.Data.SQLite;
//using System.Windows.Forms;

namespace Nmf2WavConvertor
{
    class Nmf2Wav
    {
        static long count;
        private string m_strMessage = "";

        public string GetMessage
        {
            get
            {
                return m_strMessage;
            }
        }

        private bool m_bResult = true;

        public bool BResult
        {
            get { return m_bResult; }
        }



        private int curIndex = -1;
        private readonly object locker = new object();



        public bool Nmf2WavMain(string source, string destinationFolder, int bulkSize)
        {

            if (!Directory.Exists(destinationFolder))
            {
                try
                {
                    Directory.CreateDirectory(destinationFolder);
                }
                catch (Exception ex)
                {
                    m_strMessage = string.Format("Nmf2Wav.Nmf2WavMain: Cannot create Direcorty [{0}] Error: {1}.",
                                                 destinationFolder,ex);
                    PSLog.Current.Error(m_strMessage);
                    m_bResult = false;
                    return false;
                }
            }

            if(File.Exists(source))//Convert one file.
            {
                var extension = Path.GetExtension(source);
                if (extension != null && extension.ToLower() == ".nmf")
                {
                    ConvertNmfFile(source, destinationFolder);
                    int curNum = RegistryHelper.GetItemsNumValue();
                    RegistryHelper.SetItemsNumValue(++curNum);
                }
                else
                {
                    m_strMessage = string.Format(@"Nmf2Wav.Nmf2WavMain: The File source: {0} is not nmf.", source);
                    PSLog.Current.Error(m_strMessage);
                    m_bResult = false;
                    return false;
                }
            }
            else if (Directory.Exists(source))// Convert all files in directory.
            {
                ConvertNmfFolder(source, destinationFolder, bulkSize);
            }
            else
            {
                m_strMessage = string.Format(@"Nmf2Wav.Nmf2WavMain: Direcorty\File source: {0} does not exist.", source);
                PSLog.Current.Error(m_strMessage);
                m_bResult = false;
                return false;
            }

            return true;
        }

        public void ConvertNmfFolder(string sSourceFolder, string sDestinationFolder, int bulkSize)
        {
            SQLiteDb.CreateSQLiteDatabase(sSourceFolder);
            //string[] arrNmfFiles = Directory.GetFiles(sSourceFolder, "*.nmf");
            var res = SQLiteDb.SQLExecuteScalarQuery("select count(*) as count from Files where Status=0;");
            var countTotal = int.Parse(res.ToString());
            string[] arrNmfFiles = SQLiteDb.GetFileNames("select FileName from Files where Status=0;");

            Task[] tasks = new Task[bulkSize];

            count = 0;

            for (var i = 0; i < bulkSize; i++)
            {
                tasks[i] = processTask(arrNmfFiles, sDestinationFolder);
            }
            Task.WaitAll(tasks);
            int curNum = RegistryHelper.GetItemsNumValue();
            var message1 = string.Format("{0} items processed", countTotal);
            PSLog.Current.Info(message1);
            Reporter.WriteLine(message1);
            var resFailed = SQLiteDb.SQLExecuteScalarQuery("select count(*) as count from Files where Status=-1;");
            var countFailed = int.Parse(resFailed.ToString());
            var message1f = string.Format("{0} items failed", countFailed);
            PSLog.Current.Info(message1f);
            Reporter.WriteLine(message1f);
            var resSuc = SQLiteDb.SQLExecuteScalarQuery("select count(*) as count from Files where Status=1;");
            var countSuc = int.Parse(resSuc.ToString());
            var message1s = string.Format("{0} items processed successfully", countSuc);
            PSLog.Current.Info(message1s);
            Reporter.WriteLine(message1s);
            var total = curNum + count > LicenseManager.totalLicensed ? LicenseManager.totalLicensed : curNum + count;
            var message2 = string.Format("{0} items processed total", total);
            PSLog.Current.Info(message2);
            Reporter.WriteLine(message2);

            RegistryHelper.SetItemsNumValue((int)total);

        }

        private int getNewIndex(string[] arrNmfFiles)
        {
            lock (locker)
            {
                return ++curIndex < arrNmfFiles.Length ? curIndex : -1;
            }
        }

        private Task processTask(string[] arrNmfFiles, string sDestinationFolder)
        {
            return Task.Factory.StartNew(() =>
            {
                while (true)
                {
                    int ind = getNewIndex(arrNmfFiles);
                    if (ind < 0)
                    {
                        break;
                    }
                    var sNmfFile = arrNmfFiles[ind];
                    try
                    { 
                        lock (locker)
                        {
                            count++;
                            if (count >= LicenseManager.itemsLeft)
                            {
                                PSLog.Current.Error(string.Format("Licensed number of items to process is expired, skipping additional items"));
                                RegistryHelper.SetItemsNumValue(LicenseManager.totalLicensed);
                                
                                return;
                            }
                        }

                        ConvertNmfFile(sNmfFile, sDestinationFolder);

                        

                        RegistryHelper.SetItemsNumValue((int)count);
                    }
                    catch (Exception ex)
                    {

                        

                        PSLog.Current.Error(ex.Message);
                    }
                }
            });
        }

        


      

        public void ConvertNmfFile(string sNmfFileFullPath, string sDestinationFolder)
        {

            try
            {
                var sWavFile = Path.Combine(sDestinationFolder, Path.GetFileNameWithoutExtension(sNmfFileFullPath) + ".wav");                
                NiceApplications.Playback.MediaServices.Logic.MediaFileConverter.NmfToWav(sNmfFileFullPath, sWavFile);
                //MediaFileConverterMy.NmfToWav(sNmfFileFullPath, sWavFile,true,CompressionType.G729);
                m_strMessage = string.Format("Nmf2Wav.ConvertNmfFile: File {0} converted successfully.", sNmfFileFullPath);
                var m_strMessageR = string.Format("{0} converted successfully.", sNmfFileFullPath);
                SQLiteDb.SQLiteExecuteNonQuery(string.Format("update Files set Status = 1 where FileName = '{0}'", sNmfFileFullPath));
                Reporter.WriteLine(m_strMessageR);
                PSLog.Current.Info(m_strMessage);
            }//try
            catch (Exception ex)
            {
                m_strMessage = string.Format("Nmf2Wav.ConvertNmfFile: Failed converting file: [{0}]. Error - {1}", sNmfFileFullPath, ex);
                var m_strMessageR = string.Format("{0} convertion failed.", sNmfFileFullPath);
                SQLiteDb.SQLiteExecuteNonQuery(string.Format("update Files set Status = -1 where FileName ='{0}'", sNmfFileFullPath));
                Reporter.WriteLine(m_strMessageR);
                PSLog.Current.Error(m_strMessage);
                m_bResult = false;
            }//catch
        }
    }
}
