﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Nmf2WavConvertor
{
    static class Reporter
    {
        private static readonly TextWriter reportFile;
        static Reporter()
        {
            var assemblyPath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var reportDir = System.IO.Path.Combine(assemblyPath, "reports");
            if (!Directory.Exists(reportDir))
            {
                Directory.CreateDirectory(reportDir);
            }
            var fileName = string.Format("report_{0}.txt", DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss"));
            var filePath = System.IO.Path.Combine(reportDir, fileName);
            reportFile= new StreamWriter(File.Create(filePath));
        }

        public static void WriteLine(string line)
        {
            reportFile.WriteLine(line);
        }

        public static void Dispose()
        {
            reportFile.Close();
        }
    }
}
