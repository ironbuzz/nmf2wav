﻿using ProfessionalServices.Log;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Nmf2WavConvertor
{
    static class SQLiteDb
    {
        static readonly string sqLiteConnectionString;
        static readonly string dbPath;
        static readonly string dbDir;
        static SQLiteDb()
        {
            dbDir = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            dbPath = System.IO.Path.Combine(dbDir, "db.sqlite");
            sqLiteConnectionString = $"Data Source={dbPath};Version=3;";
        }

        public static bool CreateSQLiteDatabase(string sourceFolder)
        {
            try
            {
                if (File.Exists(dbPath))
                {
                    var res = SQLExecuteScalarQuery("select count(*) as count from Files where Status=0;");
                     var count = int.Parse(res.ToString());
                    if (count >0)
                    {
                        string[] arrNmfFiles1 = Directory.GetFiles(sourceFolder, "*.nmf");
                        BulkInsert(arrNmfFiles1);
                        res = SQLExecuteScalarQuery("select count(*) as count from Files where Status=0;");
                        count = int.Parse(res.ToString());
                        PSLog.Current.Info("There are {0} unprocessed items in DB, proceeding with the same DB", count);
                        return true;
                    }

                }
                PSLog.Current.Info("Backup old SQLite DB (if exists)");
                if (File.Exists(dbPath))
                {
                    File.Move(dbPath, Path.Combine(dbDir, string.Format("db_{0}.sqlite", DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss"))));
                }

                PSLog.Current.Info("Creating new SQLite DB");
                SQLiteConnection.CreateFile(dbPath);
                string query = "CREATE TABLE Files (FileName VARCHAR(100), Status INT);";
                SQLiteExecuteNonQuery(query);
                
                string[] arrNmfFiles = Directory.GetFiles(sourceFolder, "*.nmf");
                BulkInsert(arrNmfFiles);
                return true;
            }
            catch (Exception ex)
            {
                PSLog.Current.Error(ex.Message);
                return false;
            }
        }


        public static void SQLiteExecuteNonQuery(string query)
        {
            try
            {
                using (var sqLiteConnection = new SQLiteConnection(sqLiteConnectionString))
                {
                    sqLiteConnection.Open();
                    using (var command = new SQLiteCommand(query, sqLiteConnection))
                    {
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                PSLog.Current.Error(ex.Message);
            }
        }

        public static string[] GetFileNames(string query)
        {
            List<string> list = new List<string>();
            try
            {
                using (var sqLiteConnection = new SQLiteConnection(sqLiteConnectionString))
                {
                    sqLiteConnection.Open();
                    using (var command = new SQLiteCommand(query, sqLiteConnection))
                    {
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                string FileNames = (string)reader["FileName"];

                                list.Add(FileNames);
                            }
                        }
                    }
                }
                return list.ToArray<string>();
            }
            catch (Exception ex)
            {
                PSLog.Current.Error(ex.Message);
                return null;
            }
        }

        public static object SQLExecuteScalarQuery(string query)
        {
            try
            {
                using (var sqLiteConnection = new SQLiteConnection(sqLiteConnectionString))
                {
                    sqLiteConnection.Open();
                    using (var command = new SQLiteCommand(query, sqLiteConnection))
                    {
                       return command.ExecuteScalar();

                    }
                }
            }
            catch (Exception ex)
            {
                PSLog.Current.Error(ex.Message);
                return null;
            }
        }

        static void BulkInsert(string[] items)
        {
            try
            {
                using (var sqLiteConnection = new SQLiteConnection(sqLiteConnectionString))
                {
                    sqLiteConnection.Open();
                    using (var command = new SQLiteCommand(sqLiteConnection))
                    {
                        using (var transaction = sqLiteConnection.BeginTransaction())
                        {
                            for (var i = 0; i < items.Length; i++)
                            {
                                command.CommandText =
                                    string.Format("INSERT INTO Files (FileName, Status) SELECT '{0}',0 WHERE NOT EXISTS(SELECT 1 FROM Files WHERE FileName = '{0}');", items[i]);

                                command.ExecuteNonQuery();
                            }

                            transaction.Commit();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                PSLog.Current.Error(ex.Message);
            }
        }
    }
}
