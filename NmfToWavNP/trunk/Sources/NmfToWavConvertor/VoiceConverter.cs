using System;
using NiceApplications.Playback.MediaServices.Logic;
using NiceApplications.Playback.PlayList.Logic;
using NiceApplications.Playback.Streaming.Common;
using NiceApplications.Playback.Utils;

namespace Nmf2WavConvertor
{
    /// <summary>
    /// Summary description for VoicePacketConverter.
    /// </summary>
    internal class VoiceConverter
    {
        #region Memebers
        private readonly bool m_isSourceUncompressed;
        private readonly bool m_isDestUncompressed;
        private readonly CompressionType m_sourceCompression;
        private readonly CompressionType m_destCompression;

        private readonly bool m_isInit;
        private bool m_isNeedDecoding;
        private bool m_isNeedEncoding;
        private readonly IAudioDecoder m_decoder;
        private readonly IAudioEncoder m_encoder;
        readonly AudioProcessing m_audioProcessing = new AudioProcessing();
        #endregion

        #region Constructor
        public VoiceConverter(
            bool isSourceUncompressed, CompressionType sourceCompression,
            bool isDestUncompressed, CompressionType destCompression)
        {
            m_isSourceUncompressed = isSourceUncompressed;
            m_isDestUncompressed = isDestUncompressed;
            m_sourceCompression = sourceCompression;
            m_destCompression = destCompression;

            SetIsNeedDecodeEncode();
            // Init decoder
            if (m_isNeedDecoding)
            {
                m_decoder = DecoderFactory.GetDecoder(sourceCompression);
                m_decoder.Init();
            }
            // Init encoder
            if (m_isNeedEncoding)
            {
                switch (destCompression)
                {
                    case CompressionType.PCM_A_LAW: m_encoder = new G711ALawDecoder(); break;
                    case CompressionType.PCM_MU_LAW: m_encoder = new G711MuLawDecoder(); break;
                    case CompressionType.G729: m_encoder = new G729Decoder(); break;
                    default: throw new Exception("No encoder is defined for NMF compression "
                                 + destCompression.ToString());
                }
                m_encoder.InitEncoder();
            }

            m_isInit = true;
        }
        #endregion

        #region Public Functions
        public byte[] Convert(byte[] sourceBuffer)
        {
            byte[] linearBuffer;
            byte[] destBuffer;

            if (!m_isInit)
                throw new Exception("VoicePacketConverter was not initialized.");

            // Convert to linear
            if (m_isNeedDecoding)
            {
                m_decoder.DecodeBuffer(sourceBuffer, out linearBuffer);
                if (linearBuffer == null)
                    throw new Exception("Failed to decode buffer.");
            }
            else
                linearBuffer = sourceBuffer;

            // Align buffer
            linearBuffer = AlignUncompressedBuffer(linearBuffer);

            // Convert to destination compression 
            if (m_isNeedEncoding)
            {
                m_encoder.EncodeBuffer(linearBuffer, out destBuffer);
                if (destBuffer == null)
                    throw new Exception("Failed to encode buffer.");
            }
            else
                destBuffer = linearBuffer;

            return destBuffer;
        }
        public byte[] Convert(IAudioPacket audioPacket, Media media)
        {
            byte[] linearBuffer;
            byte[] destBuffer;

            // Convert to linear
            if (m_isNeedDecoding || !audioPacket.Active)
            {
                LinData linearData = m_audioProcessing.DecodePacket(
                    audioPacket, media.StartPosition, media.StopPosition);
                linearBuffer = linearData.Data;
            }
            else
                linearBuffer = audioPacket.Data;

            // Align buffer
            linearBuffer = AlignUncompressedBuffer(linearBuffer);

            // Convert to destination compression 
            if (m_isNeedEncoding)
            {
                m_encoder.EncodeBuffer(linearBuffer, out destBuffer);
                if (destBuffer == null)
                    throw new Exception("Failed to encode buffer.");
            }
            else
                destBuffer = linearBuffer;

            return destBuffer;
        }
        #endregion

        #region Helper Functions
        void SetIsNeedDecodeEncode()
        {
            if (m_isSourceUncompressed && m_isDestUncompressed)
            {
                // Both source and destination are uncompressed - no need to encode or decode
                m_isNeedDecoding = false;
                m_isNeedEncoding = false;
                return;
            }

            if (m_isSourceUncompressed)
            {
                // Source is not compressed - need only encoding
                m_isNeedDecoding = false;
                m_isNeedEncoding = true;
                return;
            }

            if (m_isDestUncompressed)
            {
                // Destination is not compressed - need only decoding
                m_isNeedDecoding = true;
                m_isNeedEncoding = false;
                return;
            }

            if (m_sourceCompression == m_destCompression)
            {
                // Source and destination compression are identical - no need to encode or decode
                m_isNeedDecoding = false;
                m_isNeedEncoding = false;
            }
            else
            {
                // Source and destination have different compressions - need to encode and decode
                m_isNeedDecoding = true;
                m_isNeedEncoding = true;
            }
        }
        private byte[] AlignUncompressedBuffer(byte[] linearBuffer)
        {
            if (m_encoder == null)
                return linearBuffer;

            // return if data length is aligned
            if (linearBuffer.Length % m_encoder.UncompressedSampleSize == 0)
                return linearBuffer;

            // Create new data and copy given data to new data
            int sampleCount = linearBuffer.Length / m_encoder.UncompressedSampleSize;
            var newData = new byte[m_encoder.UncompressedSampleSize * (sampleCount + 1)];
            Array.Copy(linearBuffer, newData, linearBuffer.Length);

            // Add zeros at the end of the new data
            for (int i = linearBuffer.Length; i < newData.Length; i++)
                newData[i] = 0;

            return newData;
        }
        #endregion
    }
}
