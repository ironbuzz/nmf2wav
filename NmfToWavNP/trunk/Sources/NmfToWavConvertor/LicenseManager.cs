﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Dynamic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;


namespace Nmf2WavConvertor
{
    public class DynamicJsonObject : DynamicObject
    {
        public IDictionary<string, object> Dictionary { get; set; }

        public DynamicJsonObject(IDictionary<string, object> dictionary)
        {
            Dictionary = dictionary;
        }

     

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            if (!Dictionary.ContainsKey(binder.Name))
            {
                result = null;
                return false;
            }

            result = Dictionary[binder.Name];

            if (result is IDictionary<string, object>)
            {
                result = new DynamicJsonObject(result as IDictionary<string, object>);
            }
            else if (result is ArrayList && (result as ArrayList).Count > 0 &&
                     (result as ArrayList)[0] is IDictionary<string, object>)
            {
                result =
                    new List<DynamicJsonObject>(
                        (result as ArrayList).ToArray()
                                             .Select(
                                                 x => new DynamicJsonObject(x as IDictionary<string, object>)));
            }
            else if (result is ArrayList)
            {
                result = new List<object>((result as ArrayList).ToArray());
            }

            //return this.Dictionary.ContainsKey(binder.Name);
            return true;
        }
    }
    public class DynamicJsonConverter : JavaScriptConverter
    {
        public override object Deserialize(IDictionary<string, object> dictionary, Type type, JavaScriptSerializer serializer)
        {
            if (dictionary == null)
                throw new ArgumentNullException(nameof(dictionary));

            return type == typeof(object) ? new DynamicJsonObject(dictionary) : null;
        }

        public override IDictionary<string, object> Serialize(object obj, JavaScriptSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override IEnumerable<Type> SupportedTypes => new ReadOnlyCollection<Type>(new List<Type>(new[] { typeof(object) }));
    }
    public class LicenseManager
    {
        public static int totalLicensed;
        public static int itemsLeft;

        public string Customer { get; private set; }
        public string MacAddress { get; private set; }
        public int NumberOfItems { get; private set; }
        public bool IsPermanent { get; private set; }
        public DateTime ExpirationDate { get; private set; }
        public bool IsValid { get; private set; }
        public string Why { get; private set; }
        private readonly string m_licenseFile;
        private static readonly object s_lock = new object();
        private static LicenseManager s_lmngr;
        public ManualResetEvent Stop { get; set; }

        private LicenseManager(string file)
        {
            m_licenseFile = file;
            if (File.Exists(file))
            {
                Read();
            }
            else
            {
                Why = "License file not found!";
            }
        }

        public static LicenseManager GetInstance(string file)
        {
            if (s_lmngr == null)
            {
                lock (s_lock)
                {
                    if (s_lmngr == null)
                    {
                        s_lmngr = new LicenseManager(file);
                    }
                }

            }
            return s_lmngr;
        }

        private void Read()
        {
            try
            {
                var lines = File.ReadAllLines(m_licenseFile);
                if (lines.Length != 1)
                {
                    IsValid = false;
                    Why = "Bad license file";
                    return;
                }
                if (string.IsNullOrEmpty(lines[0]))
                {
                    IsValid = false;
                    Why = "Bad license file";
                    return;
                }
                var decrypted = StringCipher.Decrypt(lines[0], GetType().Name);
                dynamic json = StringToJson(decrypted);
                Customer = json.customer;
                NumberOfItems = int.Parse(json.itemNumber);
                MacAddress = json.macAddress;
                IsPermanent = bool.Parse(json.isPermanent);
                if (!IsPermanent)
                {
                    ExpirationDate = DateTime.Parse(json.expiration, CultureInfo.InvariantCulture);
                }
                var localMachineMacAddress = GetLocalMachineMacAddress();
                if (string.IsNullOrEmpty(MacAddress) ||
                    (string.IsNullOrEmpty(localMachineMacAddress)) ||
                    (!MacAddress.Equals(localMachineMacAddress)))
                {
                    IsValid = false;
                    Why = "Wrong MAC address";
                    return;
                }
                if (!IsPermanent && ExpirationDate > DateTime.Now)
                {
                    IsValid = true;
                    Task.Factory.StartNew(() =>
                    {
                        var now = DateTime.Now;
                        var evn = Stop ?? new ManualResetEvent(false);
                        var res = evn.WaitOne(TimeSpan.FromTicks((ExpirationDate - now).Ticks));
                        if (!res) //exit by timeout
                        {
                            Process.GetCurrentProcess().Kill();
                        }
                    }
                    );
                }
                if (!IsPermanent && ExpirationDate < DateTime.Now)
                {
                    Why = "License expired";
                }
                if (IsPermanent)
                {
                    IsValid = true;
                }
            }
            catch (Exception ex)
            {
                IsValid = false;
                Why = "Bad license file - " + ex.Message;
            }
        }

        public bool CreateLicense(string file, string customer, string macAddress, bool isPermanent = true, DateTime expiration = default(DateTime))
        {
            try
            {
                var dict = new Dictionary<string, string>();


                dict["customer"] = customer;
                dict["macAddress"] = macAddress;
                dict["isPermanent"] = isPermanent ? "true" : "false";
                dict["expiration"] = expiration.ToString(CultureInfo.InvariantCulture);
                var entries = dict.Select(d =>
                        string.Format("\"{0}\": \"{1}\"", d.Key, string.Join(",", d.Value)));
                var json = "{" + string.Join(",", entries) + "}";

                var encrypted = StringCipher.Encrypt(json, GetType().Name);
                if (encrypted != null)
                {
                    File.WriteAllText(file, encrypted);
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return false;
        }

        public static dynamic StringToJson(string json)
        {
            using (var reader = new JsonTextReader(new StringReader(json)))
            {
                try
                {
                    while (reader.Read())
                    { }
                }
                catch (Exception ex)
                {
                    throw new Exception(string.Format("Failed to load JSON string:{0} Error:{1}", json, ex.Message));
                }
            }

            var jss = new JavaScriptSerializer();
            jss.RegisterConverters(new JavaScriptConverter[] { new DynamicJsonConverter() });
            dynamic result = jss.Deserialize(json, typeof(object));
            return result;
        }

        public static string GetLocalMachineMacAddress()
        {
            return NetworkInterface.GetAllNetworkInterfaces().Where(nic =>
                nic.OperationalStatus == OperationalStatus.Up).Select(nic => nic.GetPhysicalAddress().ToString()).FirstOrDefault();
        }
    }
}
