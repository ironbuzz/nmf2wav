﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;
using System.Security.AccessControl;

namespace Nmf2WavConvertor
{
    public static class RegistryHelper
    {
        static readonly string NumKeyPath = "SOFTWARE\\Ironbuzz\\NMF2WAV";
        static readonly string NumKeyName = "NU";
        static public void CreateRegistryKey()
        {
            try
            {
                RegistryKey rkey2 = Registry.LocalMachine.CreateSubKey(NumKeyPath);
                rkey2.SetValue(NumKeyName, StringCipher.Encrypt("0", NumKeyPath), RegistryValueKind.String);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        static public bool RegistryKeyExists()
        {
            try
            {
                RegistryKey key = Registry.LocalMachine.OpenSubKey(NumKeyPath);
                if (key != null)
                {
                    Object o = key.GetValue(NumKeyName);
                    if (o != null)
                    {
                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        static public int GetItemsNumValue()
        {
            try
            {
                RegistryKey key = Registry.LocalMachine.OpenSubKey(NumKeyPath);
                if (key != null)
                {
                    Object o = key.GetValue(NumKeyName);
                    if (o != null)
                    {
                        var encrypted = o.ToString();
                        var decrypted = StringCipher.Decrypt(encrypted, NumKeyPath);
                        return int.Parse(decrypted);
                    }
                }
                return 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        static public void SetItemsNumValue(int value)
        {
            try
            {
                RegistryKey key = Registry.LocalMachine.OpenSubKey(NumKeyPath, true);
                if (key != null)
                {
                    key.SetValue(NumKeyName, value.ToString());
                  
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
