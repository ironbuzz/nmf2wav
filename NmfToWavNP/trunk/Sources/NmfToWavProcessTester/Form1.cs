using System;
using System.Windows.Forms;
using System.Diagnostics;

namespace MmfToWavProcessTester
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ProcessStartInfo psinice1, psinice2, psinice3, psinice4, psinice5, psinice6;

            psinice1 = new ProcessStartInfo(@"C:\Program Files\Nice Systems\NICE Nmf2Wav Convertor\Nmf2WavConvertor.exe");

            psinice1.CreateNoWindow = true;

            psinice1.WindowStyle = ProcessWindowStyle.Hidden;

            psinice1.UseShellExecute = true;

            psinice1.WorkingDirectory = @"c:\Temp\";



            psinice2 = new ProcessStartInfo(@"C:\Program Files\Nice Systems\NICE Nmf2Wav Convertor\Nmf2WavConvertor.exe");

            psinice2.CreateNoWindow = true;

            psinice2.WindowStyle = ProcessWindowStyle.Hidden;

            psinice2.UseShellExecute = true;

            psinice2.WorkingDirectory = @"c:\Temp\";



            psinice3 = new ProcessStartInfo(@"C:\Program Files\Nice Systems\NICE Nmf2Wav Convertor\Nmf2WavConvertor.exe");

            psinice3.CreateNoWindow = true;

            psinice3.WindowStyle = ProcessWindowStyle.Hidden;

            psinice3.UseShellExecute = true;

            psinice3.WorkingDirectory = @"c:\Temp\";



            psinice4 = new ProcessStartInfo(@"C:\Program Files\Nice Systems\NICE Nmf2Wav Convertor\Nmf2WavConvertor.exe");

            psinice4.CreateNoWindow = true;

            psinice4.WindowStyle = ProcessWindowStyle.Hidden;

            psinice4.UseShellExecute = true;

            psinice4.WorkingDirectory = @"c:\Temp\";



            psinice5 = new ProcessStartInfo(@"C:\Program Files\Nice Systems\NICE Nmf2Wav Convertor\Nmf2WavConvertor.exe");

            psinice5.CreateNoWindow = true;

            psinice5.WindowStyle = ProcessWindowStyle.Hidden;

            psinice5.UseShellExecute = true;

            psinice5.WorkingDirectory = @"c:\Temp\";



            psinice6 = new ProcessStartInfo(@"C:\Program Files\Nice Systems\NICE Nmf2Wav Convertor\Nmf2WavConvertor.exe");

            psinice6.CreateNoWindow = true;

            psinice6.WindowStyle = ProcessWindowStyle.Hidden;

            psinice6.UseShellExecute = true;

            psinice6.WorkingDirectory = @"c:\Temp\";


            psinice1.Arguments = @"C:\Temp\1.nmf c:\Temp\";

            psinice2.Arguments = @"C:\Temp\2.nmf c:\Temp\";

            psinice3.Arguments = @"C:\Temp\3.nmf c:\Temp\";

            psinice4.Arguments = @"C:\Temp\4.nmf c:\Temp\";

            psinice5.Arguments = @"C:\Temp\5.nmf c:\Temp\";

            psinice6.Arguments = @"C:\Temp\1000.nmf c:\Temp\";



            Process process1, process2, process3, process4, process5, process6;

            process1 = Process.Start(psinice1);
            
            process2 = Process.Start(psinice2);

            process3 = Process.Start(psinice3);

            process4 = Process.Start(psinice4);

            process5 = Process.Start(psinice5);

            process6 = Process.Start(psinice6);
           
            process1.WaitForExit();
            MessageBox.Show(process1.ExitCode.ToString());

            process1.Dispose();

            process2.WaitForExit();
            MessageBox.Show(process2.ExitCode.ToString());

            process2.Dispose();

            process3.WaitForExit();
            MessageBox.Show(process3.ExitCode.ToString());

            process3.Dispose();

            process4.WaitForExit();
            MessageBox.Show(process4.ExitCode.ToString());

            process4.Dispose();

            process5.WaitForExit();
            MessageBox.Show(process5.ExitCode.ToString());

            process5.Dispose();

            process6.WaitForExit();
            MessageBox.Show(process6.ExitCode.ToString());

            process6.Dispose();
        }
    }
}