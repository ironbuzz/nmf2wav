using System;
using System.Collections.Generic;
using System.Text;

namespace Versions
{

    public class Version
    {
        private const string _major = "9";
        private const string _minor = "10";
        private const string _build = "0";
        private const string _revision = "4";
        public const string version =
            _major + "."
            + _minor + "."
            + _build + "."
            + _revision;
    }
}
